#!/bin/sh
set -e

TMP_FOLDER="${TMP_FOLDER:-/tmp/es}"
OUTPUT_FOLDER="${OUTPUT_FOLDER:-/tmp/out}"

mkdir -p $TMP_FOLDER $OUTPUT_FOLDER
elasticdump --input=$DUMP_INPUT --output=$TMP_FOLDER/mapping.json --type=mapping > $TMP_FOLDER/mapping.log
elasticdump --input=$DUMP_INPUT --output=$TMP_FOLDER/data.json --type=data > $TMP_FOLDER/data.log

FILENAME="kibana-$(date +%y-%m-%d_%H-%M-%S).tar.gz"
tar -zcvf $OUTPUT_FOLDER/$FILENAME --strip=1 -C $TMP_FOLDER .

echo "Done! File $FILENAME was created."
