# Elasticsearch dump
Projekt slouží k exportu dashboardů z Kibany, které jsou uložené v databázi Elasticsearch.

# Jak zpustit zálohování?

```bash
docker build -t elasticdump .
docker run -v MY_FOLDER:/tmp/out -e DUMP_INPUT="http://MY-ELASTICSEARCH-HOST:9200/.kibana" elasticdump
```
uloží soubor to složky `MY_FOLDER`.

# Obnova Kibany

POZOR! Tímto krokem smažete aktualní dashboardy v Kibaně!

**Obnovení mappingu:**
```
docker run --rm taskrabbit/elasticsearch-dump \
  --input=/my/folder/mapping.json \
  --output=http://elasticsearch:9200/.kibana \
  --type=mapping
```

**Obnovení dat:**
```
docker run --rm taskrabbit/elasticsearch-dump \
  --input=/my/folder/data.json \
  --output=http://elasticsearch:9200/.kibana \
  --type=data
```

Více na [elasticsearch-dump](https://github.com/taskrabbit/elasticsearch-dump#use).
