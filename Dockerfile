FROM node:12-alpine

RUN npm install elasticdump -g

COPY run.sh /run.sh

CMD /run.sh
